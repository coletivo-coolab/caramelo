

/*****************************************************************************
Arduino library handling ping messages for the esp8266 platform

MIT License

Copyright (c) 2018 Alessio Leoncini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

// the energy wires must be connected to NC and COM!

//uncomment below if you're using i2s
//apague as barras no início da linha abaixo se você estiver usando o relay com i2c como o qwiic
//#define USING_I2C 1

#define MY_SSID "wifissid"
#define MY_PSK  "wifipass"

#define TEST_HOST "8.8.8.8"


#ifndef USING_I2C
#define RELAY_PIN 0
#endif

#ifdef USING_I2C
#include <SparkFun_Qwiic_Relay.h>
#include <Wire.h>
#define RELAY_ADDR 0x18 // Alternate address 0x19
Qwiic_Relay relay(RELAY_ADDR); 
#endif

#include <Pinger.h>
#include <ESP8266WiFi.h>



extern "C"
{
  #include <lwip/icmp.h> // needed for icmp packet definitions
}

// Set global to avoid object removing after setup() routine
Pinger pinger;




void setup()
{  

  
  // Begin serial connection at 9600 baud
  Serial.begin(9600);
  
  // Connect to WiFi access point
  bool stationConnected = WiFi.begin(
  MY_SSID,
  MY_PSK);

  // Check if connection errors
  if(!stationConnected)
  {
    Serial.println("Error, unable to connect specified WiFi network.");
  }
  
  // Wait connection completed
  Serial.print("Connecting to AP...");
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.print("Ok\n");
  
  pinger.OnReceive([](const PingerResponse& response)
  {
    if (response.ReceivedResponse)
    {
      Serial.printf(
        "Reply from %s: bytes=%d time=%lums TTL=%d\n",
        response.DestIPAddress.toString().c_str(),
        response.EchoMessageSize - sizeof(struct icmp_echo_hdr),
        response.ResponseTime,
        response.TimeToLive);
    }
    else
    {
      Serial.printf("Request timed out.\n");
    }

    // Return true to continue the ping sequence.
    // If current event returns false, the ping sequence is interrupted.
    return true;
  });
  
  pinger.OnEnd([](const PingerResponse& response)
  {
    // Evaluate lost packet percentage
    float loss = 100;
    if(response.TotalReceivedResponses > 0)
    {
      loss = (response.TotalSentRequests - response.TotalReceivedResponses) * 100 / response.TotalSentRequests;
    }
    
    // Print packet trip data
    Serial.printf(
      "Ping statistics for %s:\n",
      response.DestIPAddress.toString().c_str());
    Serial.printf(
      "    Packets: Sent = %lu, Received = %lu, Lost = %lu (%.2f%% loss),\n",
      response.TotalSentRequests,
      response.TotalReceivedResponses,
      response.TotalSentRequests - response.TotalReceivedResponses,
      loss);

    // Print time information
    if(response.TotalReceivedResponses > 0)
    {
      Serial.printf("Approximate round trip times in milli-seconds:\n");
      Serial.printf(
        "    Minimum = %lums, Maximum = %lums, Average = %.2fms\n",
        response.MinResponseTime,
        response.MaxResponseTime,
        response.AvgResponseTime);
    }
    
    // Print host data
    Serial.printf("Destination host data:\n");
    Serial.printf(
      "    IP address: %s\n",
      response.DestIPAddress.toString().c_str());
    if(response.DestMacAddress != nullptr)
    {
      Serial.printf(
        "    MAC address: " MACSTR "\n",
        MAC2STR(response.DestMacAddress->addr));
    }
    if(response.DestHostname != "")
    {
      Serial.printf(
        "    DNS name: %s\n",
        response.DestHostname.c_str());
    }

    return true;
  });

#ifdef USING_I2C  
  Wire.begin();
  // Let's see
  if(!relay.begin())
    Serial.println("Check connections to Qwiic Relay.");
  else
    Serial.println("Ready to flip some switches.");

  float version = relay.singleRelayVersion();
  Serial.print("Firmware Version: ");
  Serial.println(version);
#else
  pinMode(RELAY_PIN, OUTPUT);
  // if relay pin is off, relay is closed (connected/power on)
  digitalWrite(RELAY_PIN, LOW);
#endif

}

void loop()
{

  //check every minute.
  //TODO make sleep/wake circuit 
  //https://randomnerdtutorials.com/esp8266-deep-sleep-with-arduino-ide/
  //ESP.deepSleep(30e6);
  //connect D0 to RST
  delay(60000);
    
  if (pinger.Ping(TEST_HOST) == false)
  {

    //lets try once every minute three times no make sure it is not some instability
    delay (60000);
    if (pinger.Ping(TEST_HOST)) return; 
    delay (60000);
    if (pinger.Ping(TEST_HOST)) return; 
    delay (60000);
    if (pinger.Ping(TEST_HOST)) return; 

    
    //ok, so it looks really down. turn off router
    // Let's turn that relay off... NC is open (disconnected), NO is closed (connected)
    Serial.println("off");
#ifdef USING_I2C
    relay.turnRelayOff(); 
#else
    digitalWrite(RELAY_PIN, HIGH);
#endif    
 
    //wait 5 seconds before powering again
    delay(5000);
    // Let's turn on the relay... NO is open (disconnected), NC is closed (connected)
    Serial.println("on");
#ifdef USING_I2C
    relay.turnRelayOn(); 
#else
    digitalWrite(RELAY_PIN, LOW);
#endif    
  
    //and wait 3 minutes for the router to boot
     
    delay(3*60000);

    //now let's wait for the reconnection 
    WiFi.reconnect();
    Serial.print("Reconnecting...");
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
      Serial.print(".");
    }

    //and ping again
    if (pinger.Ping(TEST_HOST))
    {
      //carry on, it worked!
      return; //this will break and go to the beginning of the loop
    }
    
    // if it still didn't ping, the network might need more time to reconfigure everything
    // let's wait now for 15 minutes so we are not restarting the device all the time
    delay (15*60000);
    
  }

}
